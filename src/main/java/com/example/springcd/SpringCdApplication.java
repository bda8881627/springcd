package com.example.springcd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCdApplication.class, args);
    }

}
